#!/usr/bin/perl
#
# $Id$
#
# Tests the UI::Decurse::Keybindings registry.
#

use warnings;
use strict;
use utf8;
use feature 'unicode_strings';

use lib "lib";
use Test::More tests => 6;

use UI::Decurse::Keybindings;

#### FIXME: These aren't working!!!!! Why?

UI::Decurse::Keybindings::clear_keybinds();
is( scalar UI::Decurse::Keybindings::get_keybind_list(), 0, "keybinds empty" );

UI::Decurse::Keybindings::add_keybind('General', '^[', 'CANCEL_DIALOG');
is( scalar UI::Decurse::Keybindings::get_keybind_list(), 1, "ESC keybind added" );

UI::Decurse::Keybindings::add_keybind('General', '^J', 'CLICK_BUTTON');
is( scalar UI::Decurse::Keybindings::get_keybind_list(), 2, "Enter keybind added" );

is( UI::Decurse::Keybindings::get_bindings_for_key("KEY_UP"), (), "nonexistent keybind" );
is_deeply( UI::Decurse::Keybindings::get_bindings_for_key("^["), 'CANCEL_DIALOG', "ESC keybind present")
	|| diag "returned: ", explain [UI::Decurse::Keybindings::get_bindings_for_key("^[")];

UI::Decurse::Keybindings::add_keybind('General', '^[', 'OPEN_META_MENU');
is_deeply( [ UI::Decurse::Keybindings::get_bindings_for_key("^[") ], [qw/CANCEL_DIALOG OPEN_META_MENU/], "2nd ESC keybind present")
	|| diag "returned: ", explain [UI::Decurse::Keybindings::get_bindings_for_key("^[")];

