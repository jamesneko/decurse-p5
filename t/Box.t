#!/usr/bin/perl
#
# $Id$
#
# Tests the UI::Decurse::Box class.
#

use warnings;
use strict;
use utf8;
use feature 'unicode_strings';

use lib "lib";
use Test::More tests => 8;


use UI::Decurse::Box;

my $b1 = UI::Decurse::Box->new(row => 3, col => 3, width => 7, height => 7);
is( $b1->tlr(), 3, "tlr" );
is( $b1->tlc(), 3, "tlc" );
is( $b1->trr(), 3, "trr" );
is( $b1->trc(), 9, "trc" );
is( $b1->blr(), 9, "blr" );
is( $b1->blc(), 3, "blc" );
is( $b1->brr(), 9, "brr" );
is( $b1->brc(), 9, "brc" );

