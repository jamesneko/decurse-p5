#!/usr/bin/perl
#
# $Id$
#
# Tests the UI::Decurse::Hierarchical role.
#

use warnings;
use strict;
use utf8;
use feature 'unicode_strings';

use lib "lib";
use Test::More tests => 17;


# As Hierarchical is a Role, we need to mix it into a class before using it.
{
	package HierTest;
	use Moose;
	with 'UI::Decurse::Hierarchical';
}

my $h1 = HierTest->new();
ok( $h1->meta->does_role('UI::Decurse::Hierarchical'), "new() does Hierarchical" );

my $c1 = HierTest->new();
my $c2 = HierTest->new();
my $c3 = HierTest->new();
my $c4 = HierTest->new();
is( $h1->num_children(), 0, "initially no children");
ok( ! $c1->parent(), "initially no parent");

$h1->add_child($c1);
is( $h1->num_children(), 1, "now has one child");
is( $c1->parent(), $h1, "child's parent is h1");

eval {
	$h1->add_child($c1);
};
ok( $@ , "adding same child twice threw error");
is( $h1->num_children(), 1, "still has one child");
is( $c1->parent(), $h1, "child's parent is still h1");

$h1->add_child($c2);
$h1->add_child($c3);
$h1->add_child($c4);
is( $h1->num_children(), 4, "now has four childs");

$h1->remove_child($c2);
is( $h1->num_children(), 3, "removed child c2");
isnt( $c2->parent(), $h1, "c2's parent is not h1");
is( $h1->get_child(2), $c4, "get_child(index 2)");

ok( $h1->grep_children(sub { $_ == $c3 }), "grep_children(c3)");
ok( ! $h1->grep_children(sub { $_ == $c2 }), "!grep_children(c2)");

is( $h1->remove_all_children(), 3, "remove_all_children()");
is( $h1->num_children(), 0, "all children removed");
is( $c1->parent(), undef, "c1 has no parent anymore");
