package UI::Decurse;
#
# $Id$
#
# Do the basic Curses stuff that HAS TO BE DONE before all the other UI::Decurse::
# modules do their thing because otherwise you get a segfault in Curses code.
# Fun!
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.
use Moose;
use Carp;

require Curses;

use UI::Decurse::Renderer;
use UI::Decurse::Input;
use UI::Decurse::Palette;
use UI::Decurse::Text;



my @_warnings = ();
my $_renderer;
my $_input;

our $initialised = 0;
sub init
{
	croak "UI::Decurse: Already initialised once!" if ($initialised);
	$initialised = 1;
	
	Curses::initscr();	# Mixing the Curses.pm object interface with other non-object stuff makes for weird results.
	Curses::clearok(0);	# Curses will minimise screen writes; this creates junk in a few cases.
	Curses::curs_set(0);	# Invisible cursor.
	
	# We are choosing to keep a global Renderer object here for future expansion.
	$_renderer = UI::Decurse::CursesRenderer->new();
	
	# Also keep a global Input object for the same reasons. ABSTRACT ALL THE THINGS!
	UI::Decurse::CursesInput::init();	# Curses specific init magic that needs to happen.
	$_input = UI::Decurse::CursesInput->new();
	
	# initialise other submodules, too - if they require some sort of global init.
	UI::Decurse::Palette::init();
	UI::Decurse::Keybindings::init();
}


sub cleanup
{
	$initialised = 2;
	# Must happen otherwise we fuck up the user's terminal.
	Curses::endwin();
}


BEGIN {
	# Something else will have to display the warnings.
	$SIG{__WARN__} = sub { push @_warnings, $_[0]; };
	$SIG{__DIE__} = sub { cleanup(); };
}


# Access whatever Renderer has been chosen; almost certainly CursesRenderer, since that's
# the only one at time of writing, but for future expansion we're keeping it variable.
sub renderer
{
	return $_renderer;
}

sub set_renderer
{
	my ($new_renderer) = @_;
	$_renderer = $new_renderer;
}

# Access whatever Input system has been chosen; almost certainly CursesInput, since that's
# the only one at time of writing, but for future expansion we're keeping it variable.
sub input
{
	return $_input;
}

sub set_input
{
	my ($new_input) = @_;
	$_input = $new_input;
	$_input->init();	# maybe
}


#### TODO: Warnings access.


1;
