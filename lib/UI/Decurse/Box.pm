package UI::Decurse::Box;
#
# Basic "Bounding Box" or "Region" class for making working with box coordinates easier.
# Useful for layouty stuff.
#
use v5.20;
use warnings;
use strict;
use utf8;
use Carp;
use feature 'unicode_strings';	# See perldoc perlunicode.
use feature 'signatures';
no warnings 'experimental::signatures';


# Boxes have two sets of principal attributes: row/col, and height/width.
# row/col: The top-left corner of the box.
# height/width:Height and width of the box. Beware of fence-post errors, we generally consider that
# the width and height are the number of rows/columns covered completely by the box, but
# adding e.g. col() + width() will get you a column just outside the frame to the right.
# If you want the right-most edge column, use the other accessors.

sub new($class, @args)
{
	my $self = {};
	if (@args && $args[0] !~ /^\d/) {
		# Named arguments.
		croak "You seem to be using named arguments, but have supplied an odd number." if @args % 2 != 0;
		my %arg = @args;
		foreach my $n (qw/row col height width/) {
			$self->{$n} = $arg{$n};
		}
		return bless $self, $class;
		
	} else {
		# Positional arguments.
		foreach my $n (qw/row col height width/) {
			$self->{$n} = shift @args;
		}
		croak "Too many positional arguments supplied to constructor!" if @args;
		return bless $self, $class;
		
	}
}


# Primary accessors
sub row($self, @value)
{
	$self->{row} = shift @value if @value;
	return $self->{row};
}

sub col($self, @value)
{
	$self->{col} = shift @value if @value;
	return $self->{col};
}

sub height($self, @value)
{
	$self->{height} = shift @value if @value;
	return $self->{height};
}

sub width($self, @value)
{
	$self->{width} = shift @value if @value;
	return $self->{width};
}


# Get all four row,col,height,width parameters at once.
sub rchw($self)
{
	return (	$self->row, $self->col, $self->height, $self->width );
}

# Set all four row,col,height,width parameters at once.
sub set_rchw($self, $pr, $pc, $h, $w)
{
	$self->row($pr);
	$self->col($pc);
	$self->height($h);
	$self->width($w);
}


sub tlr($self, $row=undef)
{
	#### TODO: Okay, seriously considering making this a non-Moose class where we roll several custom
	   #       constructors by hand. This sort of accessor is a problem too, and perhaps we want to
	   #       allow an explicitly 'floating' bound of the Box to enable us to do layout stuff later.
	   #       of course, that then makes the default $row=undef param difficult to distinguish from
	   #       an actually-undef value... maybe we need to do a @row param much like we were doing
	   #       with @_ in the original iteration...
	return $self->row($row // ());
}
sub tlc($self, $col=undef)
{
	return $self->col($col // ());
}
sub tl($self, $maybe_tlr=undef, $maybe_tlc=undef)
{
	if (defined $maybe_tlr && defined $maybe_tlc) {
		$self->tlr($maybe_tlr);
		$self->tlc($maybe_tlc);
	}
	return ($self->tlr, $self->tlc);
}


sub brr($self, $maybe_brr=undef)
{
	if (defined $maybe_brr) {
		$self->height($maybe_brr + 1 - $self->row);
	}
	return $self->row + $self->height - 1;
}
sub brc($self, $maybe_brc=undef)
{
	if (defined $maybe_brc) {
		$self->width($maybe_brc + 1 - $self->col);
	}
	return $self->col + $self->width - 1;
}
sub br($self, $maybe_brr=undef, $maybe_brc=undef)	#huehuehue
{
	if (defined $maybe_brr && defined $maybe_brc) {
		$self->brr($maybe_brr);
		$self->brc($maybe_brc);
	}
	return ($self->brr, $self->brc);
}


# and the 'wacky corners' just for completeness.
sub trr($self, $row=undef)
{
	return $self->tlr($row // ());
}
sub trc($self, $col=undef)
{
	return $self->brc($col // ());
}
sub tr($self, $maybe_trr=undef, $maybe_trc=undef)
{
	if (defined $maybe_trr && defined $maybe_trc) {
		$self->trr($maybe_trr);
		$self->trc($maybe_trc);
	}
	return ($self->trr, $self->trc);
}


sub blr($self, $row=undef)
{
	return $self->brr($row // ());
}
sub blc($self, $col=undef)
{
	return $self->tlc($col // ());
}
sub bl($self, $maybe_blr=undef, $maybe_blc=undef)
{
	if (defined $maybe_blr && defined $maybe_blc) {
		$self->blr($maybe_blr);
		$self->blc($maybe_blc);
	}
	return ($self->blr, $self->blc);
}


1;
