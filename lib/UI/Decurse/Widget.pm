package UI::Decurse::Widget;
#
# $Id: Widget.pm 518 2014-02-25 00:11:23Z james $
#
# Class for the classical UI element.
# Handles subwidgets, drawing itself, events that it is interested in, etc.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;


# Widgets have a parent() and children().
with 'UI::Decurse::Hierarchical';

# Widgets have a canonical area on screen which they have dominion over;
# gives us row(), col(), height(), width(), and coordinate translation.
with 'UI::Decurse::Frame';

# Widgets can recieve events and handle them.
# This gives us an event() method that interested subclasses can override.
# The generic Widget event handler just ignores the event and returns false, so that the FocusHandler
# will know to try the event with the widget's parent instead.
# If you want a widget to do stuff with events, you want to override event(), maybe with a 'before'
# method modifier.
with 'UI::Decurse::EventListener';



# The generic Widget draw method simply draws all of its children in order.
# It makes no attempt to impose any sort of special placement or use its children
# for special purposes.
sub draw($$)
{
	my $self = shift;
	my $renderer = shift;
	
	foreach my $child (@{ $self->children() }) {
		$child->draw($renderer);
	}
}



1;
