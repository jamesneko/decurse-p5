package UI::Decurse::Keybindings;
#
# $Id$
#
# UI::Decurse module for managing which keys are bound to what actions.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;

use UI::Decurse;


# Do e.g. 
# 'KEY_LEFT' => 'MOVE_DOWN'
# 'j' => 'MOVE_DOWN'
# '1' => 'USESKILL_SLOT_1'
# '1' => 'CONVERSATION_OPTION_1'
# 'ENTER' => 'SELECT'
# 'KP_ENTER' => 'SELECT'

# Class for our keybinding list.
{
	package Keybind;
	use Moose;
	
	# To support the same key serving different purposes depending on context, all keys
	# can have multiple keybindings associated with them.
	# For example, in General use like list widgets, menus, moving through a dialog,
	# they will be used to move the focus up or down through the list; in the case of
	# text manipulation, they are used in line edits and text areas to move the cursor
	# through the text. The Enter key may serve multiple purposes depending on what widget
	# has focus.
	#
	# With multiple keybinds allowed per keyname, UI::Decurse::Keybindings maps each key onto
	# zero or more bindnames, each of those bindnames acting as a logical 'key' event that
	# widgets will actually respond to (or not). This abstraction allows widgets to function
	# without caring about what keys were actually used to generate the event, it allows
	# single keys to serve multiple purposes, and it allows multiple physical keys to
	# perform the same action.
	#
	# To make sense out of this many-to-many mapping in a UI context, each keyname => bindname
	# pair is also assigned a human-readable Category name, grouping the bindings into
	# loose clusters to help declutter a large list. Example category names are General,
	# Text Entry, Menu Shortcuts.
	has 'category' => qw/is ro isa Str default General/;
	has 'keyname' => qw/is ro isa Str required 1/;
	has 'bindname' => qw/is ro isa Str required 1/;
}

# The master list of all keyname -> bindname pairs (also categories)
our @keybind_list;

# For convenience, this hash gets rebuilt when the keybind list changes, and gives us only
# the mapping from keyname -> [ bindnames ].
our %keybinds_for_keyname;



our $initialised = 0;
sub init
{
	croak "UI::Decurse::Keybindings: Main UI::Decurse module not yet initialised!" unless ($UI::Decurse::initialised);
	croak "UI::Decurse::Keybindings: Already initialised once!" if ($initialised);
	$initialised = 1;

	init_default_keybinds();
}

# The basic set of built-in keybinds that are relevant to UI::Decurse widgets.
sub init_default_keybinds
{
	clear_keybinds();
	
	push @keybind_list, Keybind->new(keyname => '^[', bindname => 'CANCEL');
	push @keybind_list, Keybind->new(keyname => 'KEY_ENTER', bindname => 'SELECT');
	push @keybind_list, Keybind->new(keyname => '^J', bindname => 'SELECT');

	push @keybind_list, Keybind->new(keyname => 'KEY_UP', bindname => 'MOVE_FOCUS_UP');
	push @keybind_list, Keybind->new(keyname => 'KEY_DOWN', bindname => 'MOVE_FOCUS_DOWN');
	push @keybind_list, Keybind->new(keyname => 'KEY_LEFT', bindname => 'MOVE_FOCUS_LEFT');
	push @keybind_list, Keybind->new(keyname => 'KEY_RIGHT', bindname => 'MOVE_FOCUS_RIGHT');
	push @keybind_list, Keybind->new(keyname => '^I', bindname => 'MOVE_FOCUS_NEXT');
	push @keybind_list, Keybind->new(keyname => 'KEY_BTAB', bindname => 'MOVE_FOCUS_PRIOR');

	push @keybind_list, Keybind->new(keyname => 'ALT(`)', bindname => 'TOGGLE_CONSOLE');
	push @keybind_list, Keybind->new(keyname => 'ALT(~)', bindname => 'TOGGLE_CONSOLE');
	
	push @keybind_list, Keybind->new(category => 'Text Entry', keyname => '^H', bindname => 'TEXT_BACKSPACE');
	push @keybind_list, Keybind->new(category => 'Text Entry', keyname => 'KEY_BACKSPACE', bindname => 'TEXT_BACKSPACE');
	push @keybind_list, Keybind->new(category => 'Text Entry', keyname => 'KEY_DC', bindname => 'TEXT_DELETE');
	push @keybind_list, Keybind->new(category => 'Text Entry', keyname => 'KEY_UP', bindname => 'TEXT_CURSOR_UP');
	push @keybind_list, Keybind->new(category => 'Text Entry', keyname => 'KEY_DOWN', bindname => 'TEXT_CURSOR_DOWN');
	push @keybind_list, Keybind->new(category => 'Text Entry', keyname => 'KEY_LEFT', bindname => 'TEXT_CURSOR_LEFT');
	push @keybind_list, Keybind->new(category => 'Text Entry', keyname => 'KEY_RIGHT', bindname => 'TEXT_CURSOR_RIGHT');
	push @keybind_list, Keybind->new(category => 'Text Entry', keyname => '^J', bindname => 'TEXT_NEWLINE');
	
	_rebuild_keybind_cache();
}


# Creates a new keybind in the given category for the keyname and bindname.
# Note it is possible to have multiple bindnames for a keyname, and multiple
# keynames can trigger the same bindname.
sub add_keybind($$$)
{
	my ($category, $keyname, $bindname) = @_;
	
	# Just to make sure we don't get duplicate entries in the list, remove any that's already in there.
	# The effect of this is, if you try to insert a duplicate all you can really do is change the
	# category for it.
	@keybind_list = grep { ! ($_->keyname eq $keyname && $_->bindname eq $bindname) } @keybind_list;
	
	push @keybind_list, Keybind->new(category => $category, keyname => $keyname, bindname => $bindname);
	_rebuild_keybind_cache();
}

# Removes the pair of (keyname, bindname) from the list of keybinds.
sub remove_keybind($$)
{
	my ($keyname, $bindname) = @_;
	
	@keybind_list = grep { ! ($_->keyname eq $keyname && $_->bindname eq $bindname) } @keybind_list;
	_rebuild_keybind_cache();
}


# Given a keyname, return the list of bindnames that correspond to it (or empty list).
# Used for transforming a raw keyboard event into one with appropriate bindings listed.
sub get_bindings_for_key($)
{
	my ($keyname) = @_;
	
	if ($keybinds_for_keyname{$keyname}) {
		return @{$keybinds_for_keyname{$keyname}};
	} else {
		return;
	}
}


# Return a list of UI::Decurse::Keybindings::Keybind, for UI stuff to sort through.
sub get_keybind_list
{
	return @keybind_list;
}



sub clear_keybinds
{
	@keybind_list = ();
	_rebuild_keybind_cache();
}

# Generates %keybinds_for_keyname from @keybind_list.
sub _rebuild_keybind_cache
{
	%keybinds_for_keyname = {};
	foreach my $kb (@keybind_list) {
		if ( ! defined $keybinds_for_keyname{$kb->keyname}) {
			$keybinds_for_keyname{$kb->keyname} = [];
		}
		push @{$keybinds_for_keyname{$kb->keyname}}, $kb->bindname;
	}
}



1;
