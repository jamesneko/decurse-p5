package UI::Decurse::Renderer;
#
# $Id: Renderer.pm 518 2014-02-25 00:11:23Z james $
#
# Renderer abstraction for UI::Decurse. Acts as a middleman between it and actual
# Curses drawing functions. Only deals in screen coordinates.
#
# For now, there's exactly one concrete implementation; CursesRenderer, included right
# here for convenience. IN THEORY, we could (in the future) allow for alternate renderers,
# such as some sort of libtcod-like graphical renderer.
#
# Obviously if we're abstracting output, we'll need to abstract input at some point too...
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose::Role;


requires 'set_style';
requires 'set_frame';
requires 'string';
requires 'box';
requires 'fill';
requires 'height';
requires 'width';



package UI::Decurse::CursesRenderer;
#
# Actual Curses rendering.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;


require Curses;	# No imports to prevent any namespace conflicts, keep things separate.

use UI::Decurse;
use UI::Decurse::Palette;
use UI::Decurse::Style;
use UI::Decurse::Text;
use UI::Decurse::Frame;


# We implement the 'renderer' interface. In fact, we're the only implementation.
with 'UI::Decurse::Renderer';


# The current Style used to draw with.
has '_current_style' => (
			is => 'rw',
			isa => 'UI::Decurse::Style',
			default => sub { UI::Decurse::Style->new(fg => 'default', bg => 'default') },
		);


# Sets the current style (colour pair & attributes) used to draw with.
sub set_style
{
	my $self = shift;
	my ($style) = @_;
	croak "set_style() requires a UI::Decurse::Style object!" unless (ref($style) eq "Style");
	
	# Look up the appropriate ColourPair and attribute bits, then set them all.
	my $colourpair = UI::Decurse::Palette::get_pair($style->fg // 'default', $style->bg // 'default');
	my $attrs = $style->attrs();
	UI::Decurse::Palette::set_colour($colourpair, $attrs);
	
	$self->_current_style($style);
}


# The current frame to render into, using frame co-ordinates.
has '_current_frame' => (
			is => 'rw',
			isa => 'UI::Decurse::Frame',
		);

# Sets the current frame to render into.
sub set_frame($$)
{
	my $self = shift;
	my ($frame) = @_;
	$self->_current_frame($frame);
}




# Draws a simple scalar string, using whatever the current style is.
sub string($$$$)
{
	my $self = shift;
	my $row = shift;
	my $col = shift;
	my $str = shift;
	
	# Coordinate translation.
	if ($self->_current_frame()) {
		($row, $col) = $self->_current_frame()->map_to_screen_coords($row, $col);
	}
	
	Curses::addstr($row, $col, $str);
}


# Draws a simple box, using single-width line drawing characters.
# We should really standardise on (row, col, height, width) for symmetry in parameters.
# 'chars' should be a 9-character string if you want a fill character, 8 otherwise.
# (top-left, top-right, bottom-left, bottom-right, top, bottom, left, right, fill)
# /---\
# |   |
# |   |
# |   |
# \---/
sub box
{
	my $self = shift;
	my $fc_tl_row = shift;
	my $fc_tl_col = shift;
	my $height = shift;
	my $width = shift;
	my $chars = shift // "╒╕╘╛══││ ";
	my ($tl, $tr, $bl, $br, $top, $bottom, $left, $right, $fill) = split //, $chars;

	# Coordinate translation.
	my ($sc_tl_row, $sc_tl_col) = ($fc_tl_row, $fc_tl_col);
	if ($self->_current_frame()) {
		($sc_tl_row, $sc_tl_col) = $self->_current_frame()->map_to_screen_coords($fc_tl_row, $fc_tl_col);
	}
	
	# Corners
	Curses::addstr($sc_tl_row, $sc_tl_col, $tl);
	Curses::addstr($sc_tl_row, $sc_tl_col + $width-1, $tr);
	Curses::addstr($sc_tl_row + $height-1, $sc_tl_col, $bl);
	Curses::addstr($sc_tl_row + $height-1, $sc_tl_col + $width-1, $br);
	
	# Top/Bottom
	for (my $c = $sc_tl_col + 1; $c < $sc_tl_col + $width-1; $c++) {
		Curses::addstr($sc_tl_row, $c, $top);
		Curses::addstr($sc_tl_row + $height-1, $c, $bottom);
	}
	
	# Left/Right
	for (my $r = $sc_tl_row + 1; $r < $sc_tl_row + $height-1; $r++) {
		Curses::addstr($r, $sc_tl_col, $left);
		Curses::addstr($r, $sc_tl_col + $width-1, $right);
	}
	
	# Fill
	if (defined $fill) {
		$self->fill($fc_tl_row + 1, $fc_tl_col + 1, $height - 2, $width - 2, $fill);
	}
}



# Fills a rectangular area with a (hopefully single-width) character and the current style.
# We should really standardise on (row, col, height, width) for symmetry in parameters.
# Fill character defaults to ' '.
sub fill
{
	my $self = shift;
	my $tl_row = shift;
	my $tl_col = shift;
	my $height = shift;
	my $width = shift;
	my $char = shift // ' ';

	# Coordinate translation.
	if ($self->_current_frame()) {
		($tl_row, $tl_col) = $self->_current_frame()->map_to_screen_coords($tl_row, $tl_col);
	}

	# Fill
	for (my $r = $tl_row; $r < $tl_row + $height; $r++) {
		for (my $c = $tl_col; $c < $tl_col + $width; $c++) {
			Curses::addstr($r, $c, $char);
		}
	}
}



# Get info about screen dimensions in a Renderer-agnostic way.
sub height
{
	my $self = shift;
	return Curses::LINES;
}

sub width
{
	my $self = shift;
	return Curses::COLS;
}



1;
