package UI::Decurse::Widget::Panel;
#
# $Id: Panel.pm 518 2014-02-25 00:11:23Z james $
#
# Multipurpose Widget that you might enjoy using for boxes of all varieties.
# Depending on your application, this might be a "Window", "Dialog Box", or
# simply a non-modal region that provides ancilliary information in a box.
#
# Basically, anywhere you want a box around a thing(s), put it in a Panel.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;

use UI::Decurse;


extends 'UI::Decurse::Widget';


after draw => sub ($$) {
	my $self = shift;
	my $renderer = shift;

	#### FIXME: $renderer->set_style();
	# Ensure we are working in local coordinates.
	$renderer->set_frame($self);
	
	# Draw a nice border around (that is to say, inside) ourselves.
	$renderer->box(0, 0, $self->height(), $self->width());	# Just use default chars for now, simpler.
};


#### FIXME: How to draw a 'title'? Should we own a Label widget that we paint on top?

1;
