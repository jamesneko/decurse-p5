package UI::Decurse::Widget::TestTextFrame;

use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;

use UI::Decurse;
use UI::Decurse::Text;

extends 'UI::Decurse::Widget';

my $text1 = '...You?! are, reading!  my words $ %@';
my $text2 = "我回说中文";	# wo hui shuo zhong wen, 5 cp, 5 gc, 10 cols.
my $text3 = "Jåmes";	# James with a ringed 'a'. 5 cp, 5 gc, 5 cols.
my $text4 = "Jåmes";	# James with a 'a', followed by U+030A COMBINING RING ABOVE. 6 cp, 5 gc, 5 cols.
my $ipsum = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus, ipsum non iaculis mollis, est eros euismod est, eu tempor augue erat sed sem.";

my @frags;
push @frags, TextFragment->new(
						style => UI::Decurse::Style->new(),
						text => $text1,
					);
push @frags, TextFragment->new(
						style => UI::Decurse::Style->new(),
						text => $text2,
					);
push @frags, TextFragment->new(
						style => UI::Decurse::Style->new(),
						text => $ipsum,
					);
push @frags, TextFragment->new(
						style => UI::Decurse::Style->new(),
						text => $text4,
					);

after 'draw' => sub ($$) {
	my $self = shift;
	my $renderer = shift;
	
	my $wrapper = Wrapper->new(columns => $self->width());
	$wrapper->add_text_fragment(@frags);
	$wrapper->newline();
	$wrapper->add_text_fragment(@frags);
	$wrapper->newline();
	
	UI::Decurse::Palette::set_colour("default on default");
	$renderer->set_frame($self);
	$renderer->box(-1, -1, $self->height() + 2, $self->width() + 2);	# CHEATING!
	UI::Decurse::Palette::set_colour("bold cyan on black");
	$wrapper->_test_render($self, $renderer);
	Curses::move(25, 0);
}


1;
