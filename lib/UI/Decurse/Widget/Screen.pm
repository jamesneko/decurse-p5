package UI::Decurse::Widget::Screen;
#
# $Id: Screen.pm 518 2014-02-25 00:11:23Z james $
#
# Transparent widget that covers the entire physical screen and is automatically resized if
# necessary. Each Screen is the most top-level widget of a hierarchy of widgets that belong
# together in the same modal group and presumably do not occlude one another. Screen maintains
# a reference to one subwidget as the current keyboard focus, and is responsible for propagating
# events to the currently focused widget (which will then go up the chain of parents until we
# reach the Screen again)
#
# UI::DecurseApp maintains a stack of Screens; each of these is drawn, with the topmost Screen on
# the stack coming last and occluding the others, but only the topmost Screen in the stack will
# recieve keyboard events. This way, a Screen acts as a barrier between modal dialogs. New
# Screens will be created by UI::DecurseApp when a new 'pop up' window needs to open, the Screen will
# be populated with subwidgets to enable that modal action to take place, and the Screen and
# those subwidgets will be deleted once the user closes the dialog, returning control to the
# previously-active Screen.
#
# By parenting all widgets (ultimately) to a Screen, we ensure that mapping local widget
# coordinates to screen coordinates for rendering will always work properly.
#
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;

use UI::Decurse;


# Screen is a Widget, but it is a special transparent widget that is assumed to fill
# the entire physical screen at all times.
extends 'UI::Decurse::Widget';


# Screen objects are responsible for managing which of their subwidgets currently has
# keyboard focus. This role is only consumed by Screen; the code is kept separate for
# convenience only.
with 'UI::Decurse::FocusHandler';



# Screen has a special constructor; it makes no sense to tell Screen how big it is, so
# instead we use a Moose BUILDARGS method to initialise the attributes that Frame requires.
around BUILDARGS => sub {
	my $orig  = shift;
	my $class = shift;
	unless (@_) {
		# No args passed; make some.
		return $class->$orig(
					row => 0,
					col => 0,
					height => UI::Decurse::renderer()->height(),
					width => UI::Decurse::renderer()->width(),
				);
	} else {
		# Args were passed; just behave as Widget.
		return $class->$orig(@_);
	}
};



# Override Frame's coordinate translation methods; as we are always the top-level Screen,
# we know with certainty that we are at 0,0 and have no parent.
# Note that if you are looking to map coordinates from/to a widget's Frame of reference,
# you should be calling these methods on that Widget, not here!
override map_to_screen_coords => sub ($$$) {
	my $self = shift;
	my ($fr, $fc) = @_;
	return ($fr, $fc);
};

override map_from_screen_coords => sub ($$$) {
	my $self = shift;
	my ($sr, $sc) = @_;
	return ($sr, $sc);
};





1;
