package UI::Decurse::Palette;
#
# $Id$
#
# Low-level management of COLOR_PAIRs and friendlier interface to basic colour controls.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.
use Moose;
use Carp;

# Pull in only the Curses constants we need.
use Curses 	qw/A_ATTRIBUTES A_BLINK A_BOLD A_CHARTEXT A_COLOR A_DIM A_INVIS A_NORMAL
					A_PROTECT A_REVERSE A_STANDOUT A_UNDERLINE
					COLOR_BLACK COLOR_BLUE COLOR_CYAN COLOR_GREEN COLOR_MAGENTA COLOR_RED
					COLOR_WHITE COLOR_YELLOW
					COLORS COLOR_PAIRS
					/;

use UI::Decurse;

# IDEAS:-
# it might be useful to define a few COLOR_PAIR indexes to be 'magic',
# and have the main loop re-init them as an easy 'fire' etc. animation a'la
# Ultima VII's magic colour indexes. It does update anything that's using
# that pair on the next refresh, and it seems to be not terribly demanding
# (i.e. does redraw, but there's not a huge amount of flicker).
#
# colour name aliases - e.g. 'fire' for the above, but also 'blood', 'wood', 'player' etc.
# perhaps a customisable 'stylesheet' class? for Labels to pick their highlight style out of,
# etc.

# #### TODO:-
#      Add 256 colour support to get_pair, e.g. use #RGB style hex codes.

our $has_colour = 0;

# Big hash of 'fgcol/bgcol' => Palette::ColourPair objects, up to COLOR_PAIRS.
my %colour_pairs = ();

our $initialised = 0;
sub init
{
	croak "UI::Decurse::Palette: Main UI::Decurse module not yet initialised!" unless ($UI::Decurse::initialised);
	croak "UI::Decurse::Palette: Already initialised once!" if ($initialised);
	$initialised = 1;
	return 0 unless (Curses::has_colors());
	$has_colour = 1;
	
	Curses::start_color();
	#$colourinfo =	"COLORS: " . Curses::COLORS . " "
	#				.	"COLOR_PAIRS: " . Curses::COLOR_PAIRS . " "
	#				.	"can_change_color: " . Curses::can_change_color();
	
	# use_default_colors ordinarily sets things back to terminal defaults,
	# after start_color() has mucked things up and made things grey on grey.
	# as a fun side effect, it also appears to allow us to use -1 for FG/BG in
	# a colour pair, representing the terminal default. Without this, we don't
	# get the appropriate colour when trying to use -1 in the pair.
	Curses::use_default_colors();
	
	# Give me the blackest midnight! (RGB, 0-1000)
	# gnome-terminal can't can_change_colors() sadly, which is the one place
	# we could really really use the ability to correct their crappy default!
	# At least, it can't in its default 'xterm' mode. Why that's the default,
	# I'll never know. export TERM=xterm-256color.
	Curses::init_color(Curses::COLOR_BLACK, 0, 0, 0);
	
	# COLOR_PAIR(0) is special, and is always the terminal default.
	my $defaultpair = ColourPair->new(fg => -1, bg => -1, index => 0);
	$colour_pairs{"default/default"} = $defaultpair;
	
	# bold will typically get you the 'bright' version of colours.
	# as for attrset vs attr{on,off}, the latter can be used to tweak e.g.
	# colour without changing boldedness.
	# e.g. attrset(COLOR_PAIR(1) | A_BOLD);
	
	# ncurses won't bother blanking stuff unless you tell it to; the terminal
	# will probably just leave it whatever colour it was before.
	# this also obviates the need for us to attrset() to start with.
	Curses::bkgd(Curses::COLOR_PAIR(0));

	return 1;
}


our %colours_by_name = (
		'default' => -1,
		'black' => Curses::COLOR_BLACK,
		'red' => Curses::COLOR_RED,
		'green' => Curses::COLOR_GREEN,
		'yellow' => Curses::COLOR_YELLOW,
		'blue' => Curses::COLOR_BLUE,
		'magenta' => Curses::COLOR_MAGENTA,
		'cyan' => Curses::COLOR_CYAN,
		'white' => Curses::COLOR_WHITE,
	);
		
our %attrs_by_name = (
		'normal' => Curses::A_NORMAL,
		'standout' => Curses::A_STANDOUT,
		'underline' => Curses::A_UNDERLINE,
		'reverse' => Curses::A_REVERSE,
		'blink' => Curses::A_BLINK,
		'dim' => Curses::A_DIM,
		'bold' => Curses::A_BOLD,
	);


# Keeps track of the colour pairs we have initialised in Curses and are using.
# Do not instatiate these directly outside of Palette code! Use get_pair() instead.
{
	package ColourPair;
	use Moose;
	
	has 'fg' => qw/is rw isa Int default -1/;
	has 'bg' => qw/is rw isa Int default -1/;
	has 'animated' => qw/is rw isa Bool predicate is_animated/;
	has 'index' => qw/is ro isa Int/;
	
	sub BUILD
	{
		my $self = shift;
		Curses::init_pair($self->index, $self->fg, $self->bg);
	}
}


# Returns a reference to a ColourPair - either by making a new one or looking up
# a previously registered one.
sub get_pair($$)
{
	my ($fgname, $bgname) = @_;
	
	# If a pair has been made already, just return it.
	if (defined $colour_pairs{"$fgname/$bgname"}) {
		return $colour_pairs{"$fgname/$bgname"};
	}
	
	# Otherwise, build it.
	my $fgnum = $colours_by_name{$fgname} or carp "Unknown colour requested by name: $fgname";
	my $bgnum = $colours_by_name{$bgname} or carp "Unknown colour requested by name: $bgname";
	
	my $pair = ColourPair->new(fg => $fgnum, bg => $bgnum, index => next_free_colour_index());
	$colour_pairs{"$fgname/$bgname"} = $pair;
	return $pair;
}



# Simple way to manipulate colour and style directly.
sub set_colour
{
	my $colour = shift;
	if (ref($colour) eq "ColourPair") {
		# Ah, caller has been kind enough to give us a ColourPair directly.
		my $pair = $colour;
		my $attr = shift // Curses::A_NORMAL;	# optional 2nd arg is the attr bits.
		Curses::attrset(Curses::COLOR_PAIR($pair->index) | $attr);
		
	} else {
		# A string perhaps; attempt to parse out the details.
		my ($fgname, $bgname);
		my $attr = Curses::A_NORMAL;
		foreach my $token (split /\W+/, $colour) {
			if (defined $attrs_by_name{$token}) {
				$attr |= $attrs_by_name{$token};
			} elsif ( ! defined $fgname && defined $colours_by_name{$token}) {
				$fgname = $token;
			} elsif ( ! defined $bgname && defined $colours_by_name{$token}) {
				$bgname = $token;
			}
		}
		my $pair = get_pair($fgname, $bgname);
		Curses::attrset(Curses::COLOR_PAIR($pair->index) | $attr);
	}
}


sub next_free_colour_index
{
	my $index = scalar keys %colour_pairs;
	if ($index < Curses::COLOR_PAIRS) {
		return $index;
	} else {
		croak "Ran out of colour pairs!";
		
		# If, in the future, we decide we could allow recycling of ColourPairs,
		# then we need to invalidate the previous one - I suggest a 'is_valid'
		# attribute or setting the index to -1 - to invalidate any ColourPair
		# references still held elsewhere in the code. Then, insert a new()
		# ColourPair into the %colour_pairs hash under the new fg/bg name,
		# after undefing the old one.
	}
}



1;
