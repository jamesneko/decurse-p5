package UI::Decurse::Frame;
#
# $Id: Frame.pm 544 2014-03-20 08:32:59Z james $
#
# A Frame is a role that tells us the class consuming that role exists in an area
# on the screen that we can draw into using relative co-ordinates.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose::Role;
use Carp;

use UI::Decurse::Box;	# ephemeral little objects used to talk about regions for Layout purposes.


# The top-left corner in our parent's co-ordinates. #### FIXME: I am rethinking the requiredness.
has 'row' => qw/is rw isa Int required 1/;
has 'col' => qw/is rw isa Int required 1/;

# Height and width of this Frame. Beware of fence-post errors, we generally consider that
# the width and height are the number of rows/columns covered completely by the Frame, but
# adding e.g. col() + width() will get you a column just outside the frame to the right.
# If you want the right-most edge column, you'll have to subtract 1.
has 'height' => qw/is rw isa Int required 1/;
has 'width' => qw/is rw isa Int required 1/;


# Get all four row,col,height,width parameters at once.
# Coordinates are relative to our parent frame.
sub rchw($)
{
	my $self = shift;
	return (	$self->row, $self->col, $self->height, $self->width );
}

# Set all four row,col,height,width parameters at once.
# Coordinates are relative to our parent frame.
sub set_rchw($$$$$)
{
	my $self = shift;
	my ($pr, $pc, $h, $w) = @_;
	$self->row($pr);
	$self->col($pc);
	$self->height($h);
	$self->width($w);
}

# Get all four row,col,height,width parameters at once as a Box object.
# Coordinates are relative to our parent frame.
sub box($)
{
	my $self = shift;
	return UI::Decurse::Box->new(
			row => $self->row,
			col => $self->col,
			width => $self->height,
			height => $self->width );
}

# Set all four row,col,height,width parameters at once from a Box object.
# Coordinates must be relative to our parent frame.
sub set_box($$)
{
	my $self = shift;
	my ($box) = @_;
	$self->set_rchw($box->rchw());
}


# Given Frame-relative coords, tell me what they are in the parent's co-ordinate system.
# This is used by map_to_screen_coords to go up the chain until you reach screen co-ordinates.
sub map_to_parent_coords
{
	my $self = shift;
	my ($fr, $fc) = @_;
	
	return ($fr + $self->row, $fc + $self->col);
}


# Given Parent-relative coords (possibly Screen coords), tell me what they would correspond to
# in the Frame's coordinate system.
# Note the possibility that the resulting coordinates are out of bounds for this Frame.
sub map_from_parent_coords
{
	my $self = shift;
	my ($pr, $pc) = @_;
	
	return ($pr - $self->row, $pc - $self->col);
}


# Tests if the given co-ordinates (Frame co-ordinates, naturally - we should endeavour to do as
# much as possible in Frame space) are within the bounds of this Frame.
sub contains_point
{
	my $self = shift;
	my ($fr, $fc) = @_;
	
	return ($fr >= 0 && $fc >= 0 && $fr < $self->height && $fc < $self->width);
}


# Adjusts row,col,height,width so that the Frame is contained within the bounding box
# given (in Parent coordinates)
sub constrain
{
	my $self = shift;
	my ($pr, $pc, $h, $w) = @_;	# ####TODO: constrain 'mode'?
	#### UPTO MAYBE? DO LAYOUT STUFFS needs size constraints codified
}



# The following feature requires that anything consuming Frame also consume Hierarchical, or
# anything else that provides a means to access a single parent of the current object.
# Observe this is different to perl's "require" statement.
requires 'parent';


# Since Widgets are both Hierarchical and provide a Frame, we can use a special
# Widget to represent the Screen and make methods that map any Widget's frame
# coordinates into top-level screen coordinates, and vice versa.

# Given Frame-relative coords for this widget, tell me what they are in terms
# of the top-level Screen.
sub map_to_screen_coords
{
	my $self = shift;
	my ($fr, $fc) = @_;

	my ($pr, $pc) = $self->map_to_parent_coords($fr, $fc);
	if ($self->parent()) {
		# Recurse all the way up to Screen coords!
		return $self->parent()->map_to_screen_coords($pr, $pc);
		
	} else {
		# Shrug? there should normally be a Screen as the ultimate ancestor of all Widgets,
		# we've been disconnected somehow. We can only assume that our 'parent coords' are
		# in fact, Screen coords.
		return ($pr, $pc);
	}
}

# Given absolute top-level Screen coords, tell me what they would be relative to
# this Frame.
# Note the possibility that the resulting coordinates are out of bounds for the Frame.
sub map_from_screen_coords
{
	my $self = shift;
	my ($sr, $sc) = @_;
	
	# Work backwards from the theoretical top-level Screen.
	if ($self->parent()) {
		# Recurse all the way up until we have accounted for every Frame along the way,
		# and have coords in this Widget's parent frame of reference.
		my ($pr, $pc) = $self->parent()->map_from_screen_coords($sr, $sc);
		
		# Then we can finally map those coords into this Widget's frame of reference.
		return $self->map_from_parent_coords($sr, $sc);
		
	} else {
		# Shrug? there should normally be a Screen as the ultimate ancestor of all Widgets,
		# we've been disconnected somehow. We can only assume that our 'parent coords' are
		# in fact, Screen coords, and can map them into our Widget frame of reference directly.
		return $self->map_from_parent_coords($sr, $sc);
	}
}



1;
