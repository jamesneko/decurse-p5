package UI::Decurse::EventListener;
#
# $Id: EventListener.pm 518 2014-02-25 00:11:23Z james $
#
# An EventListener is a role that tells us the class consuming that role can receive and process
# Event objects - typically keyboard events.
# Events that are not consumed are generally propagated upwards to parent EventListener objects,
# this propagation is handled via the FocusHandler (Screen) code. This is done so that
# if Widgets subclasses want to handle a few events and defer others to their superclass
# we don't get bogged down with two possible paths for the event to follow.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose::Role;
use Carp;


# The event propagation feature requires that anything consuming EventListener also 
# consume Hierarchical, or anything else that provides a means to access a single parent
# of the current object. Observe this is different to perl's "require" statement.
requires 'parent';


# Default implementation of the event handler, if you want to handle events in your own stuff you'll
# want to override this.
# The generic event handler just ignores the event and returns false, indicating the event
# was not consumed and should be passed on to anything else that wants it (via e.g. FocusHandler)
sub event
{
	my $self = shift;
	my $event = shift;

	return 0;
}




1;
