package UI::Decurse::Input;
#
# $Id: Input.pm 507 2014-01-28 07:15:36Z james $
#
# Input abstraction for UI::Decurse. Acts as a middleman between it and actual
# Curses input functions. Generates keypress events.
#
# For now, there's exactly one concrete implementation; CursesInput, included right
# here for convenience. IN THEORY, we could (in the future) allow for alternate input systems,
# such as from some sort of libtcod-like graphical window thing.
#
# UI::Decurse module that interfaces with Curses to process keyboard/mouse input and generate
# keypress events.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose::Role;

use UI::Decurse::Event;

requires 'init';

# The method to call to test if there are new keypresses awaiting our pleasure.
requires 'process_input';


# A major feature of the Input system is a temporary queue of Event objects that can build up.
# These Events won't have any keybinds assigned to it; that's done after process_input() gets
# run. Instead, these Events are just representing the raw data that's being handled by the
# input system. raw_keyname and raw_keydata will be the interesting ones set here.
has 'queue' =>	(	is => 'rw',
						isa => 'ArrayRef[UI::Decurse::Event]',
						traits  => ['Array'],
						default => sub { [] },		# Refs not allowed as default values; this is CODE ref returning new [].
						# Using Moose's Array traits, provide a bunch of standard methods for manipulating the event queue.
						# See "Native Delegations", perldoc Moose::Manual::Attributes
						handles => {
							push_queue => 'push',
							shift_queue => 'shift',
							delete_queue => 'delete',
							get_queue => 'get',

							queue_length => 'count',

							splice_queue => 'splice',
							clear_queue => 'clear',
							grep_queue => 'grep',
						},
					);




package UI::Decurse::CursesInput;
#
# $Id: Input.pm 507 2014-01-28 07:15:36Z james $
#
# UI::Decurse module that interfaces with Curses to process keyboard/mouse input and generate
# keypress events.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;

require Curses;	# No imports to prevent any namespace conflicts, keep things separate.

use UI::Decurse;
use UI::Decurse::Event;


# We implement the 'input' interface. In fact, we're the only implementation.
with 'UI::Decurse::Input';


# On top of the 'queue' methods used to provide raw key events to the rest of the system,
# the specific CursesInput module needs a 'buffer' of raw key events where keys are held before
# attempting to interpret an escape sequence out of it.
#
# Stuff only gets queued (buffered) here if we're in the escape-sequence-processing loop; once a
# sequence is resolved, it gets added to the regular queue.
has 'buffer' =>	(	is => 'rw',
							isa => 'ArrayRef[UI::Decurse::Event]',
							traits  => ['Array'],
							default => sub { [] },		# Refs not allowed as default values; this is CODE ref returning new [].
							# Using Moose's Array traits, provide a bunch of standard methods for manipulating the event queue.
							# See "Native Delegations", perldoc Moose::Manual::Attributes
							handles => {
								push_buffer => 'push',
								shift_buffer => 'shift',
								delete_buffer => 'delete',
								get_buffer => 'get',
	
								buffer_length => 'count',
	
								splice_buffer => 'splice',
								clear_buffer => 'clear',
								grep_buffer => 'grep',
							},
						);


# How long to wait (ms) before deciding there's no new input and processing the
# next game or app loop.
#
# Passing in -1 (or any negative number) will set calls to process_input() to block
# until there is input to be had.
#
# Currently, the value you pass is only accurate to 1/10ths of a second, due to
# me just using Curses::halfdelay(int ms / 100).
#
# Changing this immediately sets Curses::halfdelay().
has 'pre_input_timeout' =>	(	is => 'rw',
										isa => 'Int',
										default => 500,
										trigger => \&_pre_input_timeout_set );

# Called when pre_input_timeout is set via setter or constructor param.
sub _pre_input_timeout_set
{
	my ($self, $timeout, $old) = @_;
	$self->set_input_timeout_mode($timeout);
}

# How long to wait (ms) after processing input which may be part of a longer escape sequence
# before deciding there's no additional input and processing what we got so far.
#
# Passing in -1 (or any negative number) is invalid and stupid. I'm not going to block
# halfway through a possible escape sequence.
#
# Currently, the value you pass is only accurate to 1/10ths of a second, due to
# me just using Curses::halfdelay(int ms / 100).
#
# Changing this value only results in a call to Curses::halfdelay if an escape is received and
# the value differs from the normal pre_input_timeout.
has 'post_input_timeout' =>	(	is => 'rw',
											isa => 'Int',
											default => 500	);


# If true (default), ^C, ^Z, ^S, ^Q and pals will generate their usual interrupt & flow control
# signals, rather than generating a keypress. If false, we put Curses into raw() mode instead
# and catch those keypresses; Make sure you have a nice stable way to quit your program before
# enabling this option!
has 'allow_flow_control' =>	(	is => 'rw',
											isa => 'Bool',
											default => 1,
											trigger => \&_allow_flow_control_set );

# Called whenever allow_flow_control is set from setter or constructor param.
sub _allow_flow_control_set
{
	my ($self, $allow, $old) = @_;
	if ($allow) {
		Curses::noraw();
	} else {
		Curses::raw();
	}
}


# our list of known (to us but not necessarily Curses) escape sequences.
{
	package EscapeSequence;
	use Moose;
	
	has 'keyname' => ( is => 'ro', isa => 'Str', required => 1 );
	has 'sequence' => ( is => 'ro', isa => 'ArrayRef[Str]', required => 1 );
}
has 'escape_sequences' =>	(	is => 'rw',
										isa => 'ArrayRef[EscapeSequence]',
										builder => '_build_escape_sequences',
									);


# What to do when we get two ESCs in quick succession? It can't be an escape sequence, surely?
# (I suppose it could be an ESC followed by some other legit escape sequence like KEY_LEFT,
# but that's kinda weird.)
#
# Setting this to true (the default) makes UI::Decurse interpret ^[^[ as the user wishing to have
# the ESC key register *right now*. It eats the second ^[, so if you (for some reason) did
# an ESC quickly followed by some other escaped key, it might not register properly.
#
# Setting this to false will make UI::Decurse realise the first ^[ is the end of the first escape
# sequence (a single push of ESC), but it will keep hanging on hoping that the next ^[ is the
# start of some other escaped key. If nothing else comes in within the post_input_timeout,
# UI::Decurse's input queue will contain two ESC raw key events.
#
# ESC via the terminal is annoying like that, and there's not much we can do about it.
has 'escesc_is_esc' =>	(	is => 'rw',
									isa => 'Bool',
									default => 1,
								);


# Curses-specific magic that needs to happen at start time.
# This *might* be ok in a BUILD method, but for now I'm erring on the side of caution
# and just using a global sub that gets called once at the appropriate time. If we end up
# enabling some other input systems, we could reevaluate this.
our $initialised = 0;
sub init
{
	croak "UI::Decurse::Input: Main UI::Decurse module not yet initialised!" unless ($UI::Decurse::initialised);
	croak "UI::Decurse::Input: Already initialised once!" if ($initialised);
	$initialised = 1;


	Curses::cbreak();		# cbreak or raw switch off the 'wait for line break' interface.
								# This can be later changed by the allow_flow_control() attribute.
	Curses::keypad(1);	# gives us special keys like the F keys.
	Curses::noecho();		# input shouldn't be immediately echoed.
}


# After Moose does its constructor thing, we get this called.
sub BUILD
{
	my $self = shift;
	
	# If the caller has changed pre_input_timeout from the default in the constructor,
	# we don't need to do anything; halfdelay() has already been set. However, if it's been
	# left at the default, our trigger method hasn't been called. So, let's ensure that
	# it definitely gets called by setting it here.
	$self->pre_input_timeout($self->pre_input_timeout());
	
	# Around construction time, we also initialise our escape_sequences() via the builder
	# _build_escape_sequences(). Yay Moose!
}




# Does one tick (possibly a short delay if there's escapes involved) of input checking.
sub process_input
{
	my $self = shift;
	
	# Here, we could configure how long we'll wait for the initial character via
	# set_input_timeout_mode, but we'll assume we're always in that mode unless
	# we're in the middle of processing an escape sequence here.
	
	# Ask Curses if there's a character in the buffer.
	# Note: Curses.pm (and of course, Curses itself) is weird and can return:-
	#   -1 for 'nothing' (in the event halfdelay() is on)
	#   literal chars (in the event user types a printable ascii)
	#   ordinal char numbers (for non-ascii)
	#   larger numbers that can be interpreted with Curses::keyname() (in the event of function keys etc.)
	# This could be convenient for someone writing a quick little Curses app, but it can
	# be irritating if you try to do ord($ch) and get back ord("2") because Curses decided to return
	# the number 259 for KEY_UP. So, Beware!
	#
	# I suppose Perl isn't entirely blameless for having a Scalar type which can be both numeric and stringish.
	my $ch = Curses::getch();
	
	if ($ch == -1) {
		# Nothing was pressed while we waited. Just return.
		return;
	}
	
	# Make a 'Raw Keypress' event to put in our internal queue for it.
	my $chev = UI::Decurse::Event->new();
	
	# Make sense of the data Curses gave us and try to add a sensible key name.
	$self->annotate_raw_curses_key($ch, $chev);

	#### NOTE: On xfce4-terminal at least, Alt-keys get sent as a single ^[ that Curses doesn't
	   #       interpret, followed by the key. e.g. (^[) (j) for Alt-J. We'll need to watch for
	   #       ^[s that get to this point, then let the buffer build up and see if we can turn it
	   #       into a recognised key.
	if (ord($ch) == 27) {	# ESC, potentially the start of an escape.
		# Push it onto the escape-sequence-processing buffer and deal with it.
		$self->push_buffer($chev);
		$self->process_potential_escape_sequence();
	} else {
		# Just push it onto the queue and return immediately.
		$self->push_queue($chev);
	}
	confess "Should be something on the queue at this point!" unless ($self->queue_length());
	return;
}


# Waits around anticipating more characters after a ^[ was received, as it may indicate the
# start of an escape sequence that Curses didn't recognise for us.
sub process_potential_escape_sequence
{
	my $self = shift;
	
	# We configure how long we'll wait for the subsequent characters via
	# set_input_timeout_mode, but we must make sure to return Curses to the
	# normal pre input timeout mode after we're done here.
	$self->set_input_timeout_mode($self->post_input_timeout);

	# Keep hunting for additional key input until we
	#  a) Hit another ^[, which can't be part of this escape sequence.
	#  b) Get a -1 from Curses, indicating the post_input_timeout timed out.
	#  c) Recognise an escape sequence in the buffer.
	#  d) Find that the buffer has grown absurdly long; yield back to the app for a bit,
	#     don't know exactly what's going on in this case; someone pasting a huge escape
	#     sequence or something.
	
	while ($self->buffer_length() < 20) {
		my $ch = Curses::getch();
		
		if ($ch == -1) {
			# Timeout mid-sequence.
			# As this could be a case of user pressing ESC and then typing wantonly, we should
			# preserve everything in the queue as-is; We can't return a "UNKNOWN_ESCAPE_SEQUENCE"
			# or similar here because if it is the user pressing their WeirdButton triggering this
			# escape, followed by legitimate characters as they pound on their keyboard in
			# frustration, we can't eat those characters and return an even weirder looking
			# "Unknown Key" sequence. So pick the least sucky thing to do, and return
			# a queue full of all this random crap we've accumulated.
			
			# Empty buffer into queue and return to regular input processing.
			$self->push_queue( @{$self->buffer} );
			$self->clear_buffer();
			last;
		}
		
		# If we get another ^[, we should stop and try to interpret the current buffer as an escape
		# sequence right now, before continuing to check what's coming up next with this second
		# escape sequence.
		if (ord($ch) == 27) {
			# ESC, potentially the start of an escape... in the middle of what we thought was an escape? NO WAI
			$self->interpret_escape_sequence_authoritative();
			# Note that we interpret the above escape sequence, THEN add the ^[ that triggered
			# this to the buffer; it might be the start of another one.
		}
		
		# Make a 'Raw Keypress' event to put in our internal buffer for it.
		my $chev = UI::Decurse::Event->new();
		
		# Make sense of the data Curses gave us so far and add it to the buffer.
		$self->annotate_raw_curses_key($ch, $chev);
		$self->push_buffer($chev);

		# Reaching here, we've got another key for the escape sequence. Let's check the
		# buffer to see if there's something that looks like a known escape sequence.
		if ($self->interpret_escape_sequence()) {
			last;
		}
	}
   
	# Return to the default timeout ready for the next call to process_input.
	$self->set_input_timeout_mode($self->pre_input_timeout);
}



# Attempt to find a valid escape sequence in the buffer given to us.
# If we are successful, return true.
sub interpret_escape_sequence
{
	my $self = shift;
	
	my $buflen = $self->buffer_length;
	
	# Okay, let's check the front of the buffer against escapes we know that Curses doesn't.
	EACHSEQ: foreach my $seq ( @{$self->escape_sequences} ) {
		# We can skip trying to match this sequence if the length of the buffer is not
		# exactly right; we can do this because interpret_escape_sequence gets called
		# repeatedly as the buffer grows, we don't need to worry about extra stuff
		# at the end of the buffer.
		next if (scalar @{$seq->sequence} != $buflen);	# Not the right length, skip.
		for (my $i = 0; $i < $buflen; $i++) {
			# Compare the two key chars based on what readable name we'd give them.
			my $seqch = $seq->sequence->[$i];
			my $bufch = $self->get_buffer($i)->raw_keyname();
			next EACHSEQ if ($seqch ne $bufch);
		}
		
		# Found one that matches!
		# Rip it out of the buffer and make a raw key Event on the queue for it.
		my $seqtext = join ' ', map { $_->raw_keyname() } @{$self->buffer};
		$self->clear_buffer();
		
		my $chev = UI::Decurse::Event->new();
		$chev->raw_keyname($seq->keyname);
		$chev->raw_keydata($seqtext);
		
		# Note we are returning as soon as the buffer fills to a state which matches a sequence
		# that we know about; it's theoretically possible for some escape sequence to be the
		# prefix of another, which would be Bad as we'd never match the longer sequence with this
		# code. 
		# e.g. Alt-[ is "^[ [", but "^[ [ 1 5 ~" is "most terminals F5". For this reason I'm not
		# supporting Alt with anything other than [a-zA-Z0-9].
		# If it becomes a problem, we'll deal with it then.
		# Shift-F1 is apparently giving me ^[ O 1 ; 2 P   , crap.
		$self->push_queue($chev);
		return 1;
	}
	# Did not find any matches.
	
	return 0;
}


# Determine that the buffer given to us *is* a valid escape sequence.
# Either let interpret_escape_sequence() handle it, or just make one up.
sub interpret_escape_sequence_authoritative
{
	my $self = shift;
	
	if ($self->interpret_escape_sequence()) {
		# OK, we've already found an escape sequence on the buffer and added it to the
		# queue; nothing needs to be done.
		
	} else {
		# Just turn the whole damn thing into a "I don't know what this is but here it is"
		# escape sequence key, since process_potential_escape_sequence() assures us it looks
		# like one.
		my $seqtext = join ' ', map { $_->raw_keyname() } @{$self->buffer};
		$self->clear_buffer();
		
		my $chev = UI::Decurse::Event->new();
		$chev->raw_keyname("DECURSE_UNKNOWN_ESCAPE_SEQUENCE($seqtext)");
		$chev->raw_keydata($seqtext);
		
		$self->push_queue($chev);
	}
}





# Fill in the raw_ fields of a keyboard event generated from Curses.
# Try to add a sensible name for it.
sub annotate_raw_curses_key
{
	my $self = shift;
	my ($ch, $chev) = @_;
	
	# Filling in the raw data part is easy.
	$chev->raw_keydata($ch);
	
	# The tricky part; separating out the special keys based on what Curses tells us.
	if (length $ch > 1) {
		# It is not just a single char.
		if ($ch eq "0" || $ch > 0) {
			# It is a wholly numeric char sequence. Quite possibly, it is a special key code
			# that Curses has given us for e.g. one of the Function keys.
			if (defined Curses::keyname($ch) && Curses::keyname($ch) ne "^@") {
				# Curses knows what it is!
				$chev->raw_keyname(Curses::keyname($ch));
			} else {
				# Curses gave this to us, but doesn't know what it is. I call shenanigans.
				$chev->raw_keyname("DECURSE_UNKNOWN_KEYCODE($ch)");
			}
			
		} else {
			# It is a not-wholly-numeric char sequence.
			# Not really sure what to expect here; possibly pasted/IMEd Unicode sequence.
			$chev->raw_keyname("DECURSE_UNKNOWN_CHAR_SEQUENCE($ch)");
		}
		
	} else {
	
		# It is just a single char.
		if ($ch eq "0" || $ch > 0) {
			# It is a single numeric char.
			# This should not be passed to Curses::keyname because it will falsely claim that
			# we are looking at one of the low-numbered control characters.
			
			# In this case, I think it's okay to set the name to the key itself.
			$chev->raw_keyname($ch);
			
		} else {
			# It is a single non-numeric char.
			# Could be a key literal or an unprintable control code from e.g. Tab or Ctrl-something.
			# Unctrl seems to do the right thing wrt the name of the key here, except for the low
			# control codes where it returns ^@ for a bunch of stuff.
			if (Curses::unctrl($ch) ne "^@") {
				# Okay, we've got a decent label for it from Curses, such as ^H, or just 'n'.
				$chev->raw_keyname(Curses::unctrl($ch));
				
			} else {
				# Control code that Curses can't give us with ^-notation. So write it as 0x1a.
				$chev->raw_keyname("CONTROL_CODE(" . sprintf("%#02x", ord($ch)) . ")");
				
			}
		}
	}
	# Okay, we did our best and filled in the raw_ attributes.
}



# Used internally to switch halfdelay() mode during process_input(),
# according to pre_input_timeout and post_input_timeout attributes.
sub set_input_timeout_mode
{
	my ($self, $timeout) = @_;

	if ($timeout < 0) {
		Curses::cbreak();	# man halfdelay : "Use nocbreak to leave half-delay mode", yet we want cbreak. Sooo... yeah.
		# FIXME: Might have to reinstate raw() if this happens.
	} else {
		my $tenths_of_a_second = int $timeout / 100;
		Curses::halfdelay($tenths_of_a_second);
	}
}



# Constructs our list of known escape sequences that Curses doesn't seem to support.
sub _build_escape_sequences
{
	my $self = shift;
	my @seqs;

	foreach my $c ('a'..'z', 'A'..'Z', '0'..'9', qw/` ~/) {	
		push @seqs, EscapeSequence->new(keyname => "ALT($c)", sequence => [ '^[', $c ]);
	}
	push @seqs, EscapeSequence->new(keyname => "ALT(ENTER)", sequence => [ '^[', '^J' ]);
	
	return [ @seqs ];
}



1;
