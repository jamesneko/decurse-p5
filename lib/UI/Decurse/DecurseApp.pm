package UI::Decurse::UI::DecurseApp;
#
# $Id$
#
# Base class for your UI::Decurse application; handles the main loop and widget management.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;

use UI::Decurse;
use UI::Decurse::Input;
use UI::Decurse::Keybindings;
use UI::Decurse::Palette;
use UI::Decurse::Text;
use UI::Decurse::Widget;
use UI::Decurse::Widget::Screen;


# Set $self->exit(1) for graceful exit from the main loop.
has 'should_exit' => (is => 'rw', isa => 'Bool', writer => 'exit');



# The root of the user interface is modelled as a stack of UI::Decurse::Widget::Screen widgets.
# Each Screen is a transparent widget that automatically resizes itself when the user's
# terminal does, and each Screen manages a tree of subwidgets that are active and visible
# within that screen. The topmost Screen's widgets will occlude any widgets of screens below
# them on the stack, and only the topmost Screen gets keyboard events. Each Screen maintains
# a focused widget that is the primary recipient of keyboard events. For more detail, see
# UI::Decurse::Widget::Screen and UI::Decurse::FocusHandler.
has 'screen_stack' => (
			is => 'rw',
			isa => 'ArrayRef[UI::Decurse::Widget::Screen]',
			traits => ['Array'],
			builder => '_build_screen_stack',
			# Using Moose's Array traits, provide a bunch of standard methods for manipulating the stack of Screens
			# See "Native Delegations", perldoc Moose::Manual::Attributes
			handles => {
				_push_screen => 'push',
				_pop_screen => 'pop',
				_screen_stack_size => 'count',
				_get_screen => 'get',
			},
		);

sub _build_screen_stack
{
	my $screen0 = UI::Decurse::Widget::Screen->new();
	return [ $screen0 ];
}

# Returns the topmost UI::Decurse::Widget::Screen on the stack.
sub active_screen($)
{
	my $self = shift;

	confess "There is no screen stack!!" unless defined $self->screen_stack();
	croak "There are no screens on the stack!" unless $self->_screen_stack_size() > 0;
	return $self->_get_screen(-1);
}


# Once we get a Raw Keyboard Event from the Input system, we assign bindnames to it and
# queue it up here (where it will be immediately processed by the next stage of the UI::Decurse
# Main Loop, unless other stuff has been queueing up other kinds of events).
has 'event_queue' =>	(
								is => 'rw',
								isa => 'ArrayRef[UI::Decurse::Event]',
								traits  => ['Array'],
								default => sub { [] },		# Refs not allowed as default values; this is CODE ref returning new [].
								# Using Moose's Array traits, provide a bunch of standard methods for manipulating the event queue.
								# See "Native Delegations", perldoc Moose::Manual::Attributes
								handles => {
									push_event_queue => 'push',
									shift_event_queue => 'shift',
									delete_event_queue => 'delete',
									get_event_queue => 'get',

									event_queue_length => 'count',

									splice_event_queue => 'splice',
									clear_event_queue => 'clear',
									grep_event_queue => 'grep',
								},
							);


# Augmentable mainloop.
# If you need finer control over where your code gets inserted, it is recommended to
# do 'before' or 'after' hooks on draw(), handle_input(), etc, or just override mainloop
# with your own implementation.
sub mainloop($)
{
	my $self = shift;
	
	while ( ! $self->should_exit()) {
	
		# Render everything that needs to be rendered; All active widgets and their subwidgets.
		$self->draw(UI::Decurse::renderer());
		
		# Wait a (configurable) moment to see if there's any input.
		# #### Calls process_input as necessary to handle resize events, mouse, blah blah
		# #### Ensure to split things up so that if user wants to just grab all input events
		#      globally, they can, before said events get dispatched to widgets according to
		#      whatever focus structure we come up with.
		$self->handle_input();
		
		# If we got any keypresses, they have now been transformed into properly-annotated
		# key events with any keybinds included, and added to the UI::DecurseApp::event_queue.
		# We might also have events generated from elsewhere.
		# Process one of those events now.
		$self->process_event_queue();
		
		# User application specifics can go here by augmenting mainloop.
		inner();
	}
}


# Render all the widgets.
# If you have some special rendering to do, you may want to add an 'after' method for this
# in your own App code.
sub draw($$)
{
	my $self = shift;
	my $renderer = shift;
	
	$self->active_screen()->draw($renderer);
}


# Consults Input module for raw input events, delegates to subs to deal with them.
# You probably don't need to override this in your App code.
sub handle_input($)
{
	my $self = shift;
	
	my $input = UI::Decurse::input();
	# Check for keypresses from Curses.
	$input->process_input();
	
	# Did anything come through? Check the input queue. Note that this is completely
	# distinct to the UI::DecurseApp::event_queue.
	while ($input->queue_length()) {
		# A raw keyboard (or mouse, etc) event!
		my $raw = $input->shift_queue();
		if ($raw->raw_keyname() || $raw->raw_keydata()) {
			# This looks like keyboard input. Handle it and create App events.
			$self->handle_raw_keyboard_event($raw);
		}
		#### TODO: Mouse, resize input events
	}
}


# Transforms raw keyboard events from the Input queue into a proper Event with bindnames
# associated with it.
# Puts it onto the UI::DecurseApp event_queue.
# You definitely don't need to mess with this in your own App code.
sub handle_raw_keyboard_event($$)
{
	my $self = shift;
	my $raw = shift;
	
	# Look up the bindings associated with that key.
	my $keyname = $raw->raw_keyname();
	my @bindnames = UI::Decurse::Keybindings::get_bindings_for_key($keyname);
	
	# Annotate the keybindings into the key event. These are what client code should
	# be looking at to determine how to respond, not the raw_keyname or raw_data,
	# save for things like text entry.
	$raw->keybinds([ @bindnames ]);
	
	# Push our (now first-class) Event onto the App's main event queue.
	$self->push_event_queue($raw);
}


# Shifts one item off the event queue and performs the appropriate processing.
# This method automatically delegates to handle_keyboard_event or whatever's appropriate.
# If you feel the need to hook process_event_queue(), you may find it better to hook
# one of those instead, unless you're adding your own special event class.
sub process_event_queue($)
{
	my $self = shift;
	
	return unless $self->event_queue_length();
	
	# Shift one thing off the queue.
	my $event = $self->shift_event_queue();
	
	# What do we do with it?
	if (scalar @{$event->keybinds()} || $event->raw_keydata()) {
		# Keyboard event! Dispatch it to whatever has focus, or process global shortcuts.
		$self->handle_keyboard_event($event);
	}
	#### TODO: A mouse click event may need to be dispatched based on where the click
	# happened, a resize event gets dispatched straight to the Screen without regard
	# to focused widgets.
}


# A keyboard event has been shifted off the queue. Handle it here by dispatching
# it to whatever Screen is active.
# #### TODO: A select few keys should trigger global shortcuts.
sub handle_keyboard_event($)
{
	my $self = shift;
	my $event = shift;
	
	$self->active_screen()->pass_keyboard_event_along_focus_chain($event);
}


1;
