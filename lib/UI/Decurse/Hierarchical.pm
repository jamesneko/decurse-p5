package UI::Decurse::Hierarchical;
#
# $Id: Hierarchical.pm 518 2014-02-25 00:11:23Z james $
#
# Role defining a thing that has a parent (and therefore something that could have children).
# Consumed by Widget.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose::Role;
use Carp;


has 'parent' => 	(	is => 'rw',
							isa => 'Maybe[UI::Decurse::Hierarchical]',
							weak_ref => 1,
						);
has 'children' => (	is => 'rw',
							isa => 'ArrayRef[UI::Decurse::Hierarchical]',
							traits  => ['Array'],
							default => sub { [] },		# Refs not allowed as default values; this is CODE ref returning new [].
							# Using Moose's Array traits, provide a bunch of standard methods for manipulating the child list.
							# See "Native Delegations", perldoc Moose::Manual::Attributes
							handles => {
								# The auto-defined Array Traits subs that are mutators are kept
								# PRIVATE, because we want the only user interface that affects
								# the child list to also update the 'parent' weakref of the child.
								# User code should use add_child(), remove_child(), and remove_all_children()
								# instead of calling these.
								_push_child => 'push',
								_delete_child => 'delete',
								_splice_children => 'splice',
								_clear_children => 'clear',
								
								# Simple accessors are safe, though.
								get_child => 'get',
								num_children => 'count',
								grep_children => 'grep',
								map_children => 'map',
								
								# I don't think we have any need for user code to mess with the
								# order of children in the list; at least, we don't have any
								# user for it yet. If we do, we need to implement it separately.
								#_insert_child => 'insert',

							},
						);

# push a new child entry, 
# - automatically link its parent to this,
# - automatically unlink from previous parent if applicable.
# - it is an error to add_child something that is already our direct child.
# - it is incredibly offensive to me to add myself as my own child.
sub add_child($$)
{
	my $self = shift;
	my $child = shift;
	croak "Can't add an undef child!" unless defined $child;
	croak "Child already has this as its parent!" if ($child->parent() && $child->parent() == $self);
	croak "Attempted to add object as a direct child of itself!" if ($child == $self);
	
	# A child can have but one parent!
	my $prev_parent = $child->parent();
	if ($prev_parent) {
		$prev_parent->remove_child($child);
	}
	
	# And that is me!
	$child->parent($self);
	
	# And this is one of my children!
	$self->_push_child($child);
}


# Remove a given child entry (presuming it exists on the children list)
# Does nothing if given child not on list.
# also unsets child->parent() to keep things consistent.
# 
# Possible bug: We don't remove multiple instances of the same child,
# although such a state is a major failure in itself.
sub remove_child($$)
{
	my $self = shift;
	my $child = shift;
	croak "Can't remove an undef child!" unless defined $child;
	
	for (my $i = 0; $i < $self->num_children(); $i++) {
		if ($self->get_child($i) == $child) {
			# warn if child->parent() != $self at this point.
			carp "Removing a child from parent that does not have its parent() set correctly!" unless $child->parent() && $child->parent() == $self;
			
			# Splice that child right out!
			my $rval = $self->_splice_children($i, 1);
			
			# Keep the parent weakref updated.
			$child->parent(undef);
			return $rval;
		}
	}
	return undef;
}


# Gets rid of all this object's children. Where do they go from here?
# Who cares! As long as we make sure they have no parent(), they can get lost.
sub remove_all_children($)
{
	my $self = shift;
	my $removed = 0;
	
	for (my $i = 0; $i < $self->num_children(); $i++) {
		my $child = $self->get_child($i);
		# warn if child->parent() != $self at this point.
		carp "Removing a child from parent that does not have its parent() set correctly!" unless $child->parent() && $child->parent() == $self;
		
		# You are no son of mine!
		$child->parent(undef);
		$removed++;
	}
	$self->_clear_children();
	
	return $removed;
}


1;
