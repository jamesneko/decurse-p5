package UI::Decurse::Style;
#
# $Id: Style.pm 227 2012-09-13 07:37:12Z james $
#
# "Style" abstraction for UI::Decurse. Can define a complete or partial combo of fg/bg/attrs.
#
# Used to delimit sections of actual characters with style information in a
# Text::StyledText. Note that it can be partially defined, applying a foreground
# or boldness change without specifying what the background does, etc.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;

require Curses;	# No imports to prevent namespace conflict with our 'standout' attr.

use UI::Decurse;



# Any name that Palette supports.
has 'fg' => qw/is rw isa Str predicate has_fg/;
has 'bg' => qw/is rw isa Str predicate has_bg/;

# 0, 1, or not present to not specify.
has 'normal' => qw/is rw isa Bool predicate has_normal/;			# Enforced 'normalness'!
has 'standout' => qw/is rw isa Bool predicate has_standout/;
has 'underline' => qw/is rw isa Bool predicate has_underline/;
has 'reverse' => qw/is rw isa Bool predicate has_reverse/;
has 'blink' => qw/is rw isa Bool predicate has_blink/;
has 'dim' => qw/is rw isa Bool predicate has_dim/;
has 'bold' => qw/is rw isa Bool predicate has_bold/;



# If this Style object represents the 'current' foreground, background, and attribute
# bits, apply the passed style as a modification to this one's parameters.
# Called from StyledText.
#
# We do not directly call Palette::set_colour here, to avoid the possible waste of
# generating ColourPairs when we don't actually need them - defer that until we know
# we have characters to print with the compounded style.
sub apply
{
	my $self = shift;
	my ($other) = @_;
	
	$self->fg($other->fg()) if $other->has_fg();
	$self->bg($other->bg()) if $other->has_bg();
	
	if ($other->has_normal()) {
		# Applying normalness to a base Style means clearing that one out attr-wise,
		# then applying the new attrs.
		$self->normal($other->normal());
		if ($other->normal()) {
			$self->standout(0);
			$self->underline(0);
			$self->reverse(0);
			$self->blink(0);
			$self->dim(0);
			$self->bold(0);
		}
	}
	# Otherwise, explicitly-set/explicitly-unset as appropriate.
	$self->standout($other->standout()) if ($other->has_standout());
	$self->underline($other->underline()) if ($other->has_underline());
	$self->reverse($other->reverse()) if ($other->has_reverse());
	$self->blink($other->blink()) if ($other->has_blink());
	$self->dim($other->dim()) if ($other->has_dim());
	$self->bold($other->bold()) if ($other->has_bold());
}
	

# Gets current attrs, as applied to an optionally supplied base set of attr bits.
sub attrs
{
	my $self = shift;
	my $attr = shift // Curses::A_NORMAL;
	
	# A Style can explicitly mandate that things return to normal.
	$attr = Curses::A_NORMAL if $self->has_normal() && $self->normal();
	
	$attr |= Curses::A_STANDOUT if $self->has_standout() && $self->standout();
	$attr ^= Curses::A_STANDOUT if $self->has_standout() && ! $self->standout();
	$attr |= Curses::A_UNDERLINE if $self->has_underline() && $self->underline();
	$attr ^= Curses::A_UNDERLINE if $self->has_underline() && ! $self->underline();
	$attr |= Curses::A_REVERSE if $self->has_reverse() && $self->reverse();
	$attr ^= Curses::A_REVERSE if $self->has_reverse() && ! $self->reverse();
	$attr |= Curses::A_BLINK if $self->has_blink() && $self->blink();
	$attr ^= Curses::A_BLINK if $self->has_blink() && ! $self->blink();
	$attr |= Curses::A_DIM if $self->has_dim() && $self->dim();
	$attr ^= Curses::A_DIM if $self->has_dim() && ! $self->dim();
	$attr |= Curses::A_BOLD if $self->has_bold() && $self->bold();
	$attr ^= Curses::A_BOLD if $self->has_bold() && ! $self->bold();
	
	return $attr;
}


# Create a new Style that is a copy of this one.
sub clone
{
	# There is probably a better way I could be going about this, but I'm only
	# just learning Moose.
	my $self = shift;
	my $clone = $self->new();
	$clone->apply($self);
	return $clone;
}


1;
