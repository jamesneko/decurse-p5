package UI::Decurse::Event;
#
# $Id: Event.pm 518 2014-02-25 00:11:23Z james $
#
# Class for the basic "Something Happened" message.
#
# Things that could be events:-
#  - Keypress event: Holds a list of applicable keybinds determined from Keybinds.pm.
#                    Try to isolate things from underlying Curses constants as much as possible.
#                    Have an internal field for the raw keypress data, so a keybinding UI is
#                    more easily done (and so we don't need to map every key on every keyboard just
#                    to be able to insert characters).
#                    Raw keypress data is a *string*, to accommodate weird escape sequences and
#                    also crap like pasted Unicode and whatever the user's IME gives us.
#  - Resize event:   Technically a keycode Curses returns, but I think it best to keep it distinct.
#                    Aside from the obvious Screen resize event that Curses gives us, we may also
#                    want to send a Layout-related "your frame has been resized" event. Or maybe
#                    we'll just synchronously call resize methods, I don't know yet.
#  - Mouse event:    Clicks, possibly drags, nothing guaranteed.
#  - Signal event?:  Trap Ctrl-Z and pals by default so that we have a chance to rebind them?
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose;
use Carp;


# Names of keybinds that are applicable to the actual raw keys that generated this Event.
has 'keybinds' => (	is => 'rw',
							isa => 'ArrayRef[Str]',
							traits  => ['Array'],
							default => sub { [] },		# Refs not allowed as default values; this is CODE ref returning new [].
							# Using Moose's Array traits, provide a bunch of standard methods for manipulating the list.
							# See "Native Delegations", perldoc Moose::Manual::Attributes
							handles => {
								_push_keybind => 'push',
								
								get_keybind => 'get',
								num_keybinds => 'count',
								grep_keybinds => 'grep',
							},
						);

# Since grep_keybinds expects a code ref, this is a slightly more elegant way to check
# to see if a particular keybind is on this event's list of keybinds:-
sub has_keybind($$)
{
	my $self = shift;
	my ($bind_to_match) = @_;
	
	return scalar $self->grep_keybinds(sub { $_ eq $bind_to_match });
}

# Any "Special Key" names identified by Curses or whatever other input systems we might use.
# I.e. "KEY_F(1)", "KEY_RESIZE", "^D" etc.
has 'raw_keyname' =>	(	is => 'rw',
								isa => 'Maybe[Str]',
							);

# The raw key data that came along with this keypress event, that is associated with the keybinds.
# Really, only internal stuff should use this, excepting the case where we're getting unicode text
# pasted straight into the terminal.
has 'raw_keydata' =>	(	is => 'rw',
								isa => 'Maybe[Str]',
							);


1;
