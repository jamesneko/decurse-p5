package UI::Decurse::FocusHandler;
#
# $Id: FocusHandler.pm 512 2014-02-15 23:06:57Z james $
#
# FocusHandler is a Role only for the convenient separation of code. It is only consumed
# by UI::Decurse::Widget::Screen, in that Screen objects are the sole objects responsible for
# managing which of their subwidgets have keyboard focus and the appropriate dispatch of
# keyboard events. The rest of this comment will talk about focus management in terms of
# the Screen object that manages them, even though the actual code is in the FocusHandler role.
#
# Screens group widgets together in a number of ways; obviously, they group them visually,
# portioning out areas of the physical screen to subwidgets according to layouts and the
# widget hierarchy. They also represent a modal group where there is precisely one of their
# member widgets that is (typically) highlighted and said to have keyboard focus.
#
# There will always be at least one Screen in the stack of screens maintained by UI::DecurseApp.
# Without one, not only would you not get any widgets drawn, you would also fail to have
# any keyboard events processed unless they were some sort of global shortcut.
# Further, only the topmost Screen in the stack will ever get keyboard events; the model used
# presumes that a Screen on top of others is a modal pop-up dialog of some kind, and in
# obscuring the visibility of widgets on lower Screens, it also prevents any keyboard events
# from reaching them. This allows you to have sane behavior of the Enter and arrow keys,
# for example - you don't move your character around while navigating the inventory.
#
# Since widgets are all ultimately parented to their Screen widget, and Screens themselves
# do not have a parent, the process of handing keyboard events to widgets is fairly simple;
# The UI::DecurseApp passes the event to the Screen on the top of the stack, Screen passes the
# event to the current focused widget, and if it does not consume that event, we try passing
# it to the parent widget, and so on until we find ourselves at the top of the list of
# parents, this Screen (which does not have a parent) and so the event poofs out of existence.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.

use Moose::Role;
use Carp;


#### TODO: Everything, it seems.
has 'focus' => (	is => 'rw',
						isa => 'Maybe[UI::Decurse::Widget]',
						weak_ref => 1,
					);


# It is the responsibility of the FocusHandler to give event()s to focused widgets and
# propagate that event() up to parent() widgets if the event does not get consumed
# (i.e. if event() returns true).
sub pass_keyboard_event_along_focus_chain
{
	my $self = shift;
	my $event = shift;
	
	# start with our focus widget
	# iterate until no parent
	my $w = $self->focus();
	while ($w) {
		# See if this widget wants the event. If it does, we don't pass it on to anyone else.
		last if $w->event($event);
		
		# Move up to the parent, if there is one.
		$w = $w->parent();
	}
}



1;
