package UI::Decurse::Text;
#
# $Id$
#
# Text abstraction for UI::Decurse. Handles text marked up with colours and attributes.
#
# StyledText -> the nested structure we'd get post-markup-processing.
# TextFragment -> a flattened structure that's easier to work with as a simple list of fragments.
# Wrapper -> class to do text wrapping while keeping track of state.
#
use warnings;
use strict;
use utf8;
use feature 'unicode_strings';	# See perldoc perlunicode.
use Moose;
use Carp;

use UI::Decurse;
use UI::Decurse::Style;
use UI::Decurse::Palette;	# For testing, until there's suitable methods in Renderer.



# Nestable structure defining styled text.
{
	package StyledText;
	use Moose;
	
	has 'style' => qw/is rw isa UI::Decurse::Style required 1/;
	has 'text' => qw/is rw isa ArrayRef[Str|StyledText] required 1/;
}




# DESIGN NOTE:
# All this time, I'd been considering the final data structure as being a stream of scalars
# mixed in with widthless 'formatting codes' and getting confused about how text-scanning
# operations would interact with them.
# What I need to do is stop thinking about them as being discrete entities that exist between
# two strings, and instead interact only with TextFragment objects. These combine a string
# with a Style that applies across the whole string. They have logical operations to split themselves
# into more TextFragment objects, or maybe merge TextFragment objects. They are created by
# passing over the nested StyledText structure given above, chopping the strings up as needed
# whenever the computed Style changes, and can be further passed-over to chop them up for
# word-wrap and clipping.
# This is made substantially easier by only requiring I iterate over a list of TextFragment objects,
# comparing previous TextFragment::widths, and requesting TextFragment::splitAtWidth as need be.


#### FIXME: To solve the middle-part-of-word-highlighted problem, consider preprocessing
   #        the stream of TFs and lump together anything that shouldn't be broken.
   #        Either with a container or a flag 'no-break-after' etc on the TF itself.

# Abstraction for a simple portion of text with a uniform Style applied throughout.
# Objects of StyledText are transformed to a list of TextFragment before further processing
# is done.
{
	package TextFragment;
	use Moose;
	
	use Text::CharWidth qw(mbwidth mbswidth mblen);
	
	# This Style has presumably been precomputed and is complete, unlike the partial
	# Style objects allowed in StyledText structures.
	has 'style' => qw/is rw isa UI::Decurse::Style required 1/;
	
	# The text of a TextFragment is always a simple scalar, with the corresponding Style
	# applied to it throughout.
	has 'text' => qw/is rw isa Str required 1/;


	# Returns width of whole fragment in terms of on-screen columns used.
	sub width
	{
		my $self = shift;
		return mbswidth($self->text);
	}
	
	# How many codepoints are used in the string. This does not necessarily
	# correlate to 'characters on screen' or 'columns used' at all.
	sub length_in_code_points
	{
		my $self = shift;
		return length $self->text;
	}
	
	# Returns how many physical 'characters' a user would see, after things like combining marks are
	# taken into account. All other character-based stuff in Perl generally deals with code points.
	sub length_in_grapheme_clusters
	{
		my $self = shift;
		my $length = () = $self->text =~ /\X/g;
		return $length;
	}
	
	# Line breaking:
	# ==============
	# Sample: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean luctus, ipsum non iaculis mollis, est eros euismod est, eu tempor augue erat sed sem.
	# ---------+---------+---------+
	# Lorem ipsum dolor sit amet, 
	# consectetur adipiscing elit. 
	# Aenean luctus, ipsum non 
	# iaculis mollis, est eros 
	# euismod est, eu tempor augue 
	# erat sed sem.
	
	# If this returns undef, it means there was nothing long enough to wrap past the column width.
	# If this returns 0, it means there was, starting from cp 0, a single unbroken word that
	# falls off the defined column width all by itself, which warrants special behaviour.
	# Otherwise it should return a cp pos that will break the fragment to a width <= $column_to_split_at.
	sub get_word_break_point
	{
		my $self = shift;
		my ($column_to_split_at) = @_;
		
		my $running_width = 0;
		
		my $str = $self->text();
		while (1) {
			my $initialpos = pos($str) // 0;	# Remember pos before regex as we need to try both \w and \W.
			if ($str =~ /(\G\w+)/g) {
				my $word_start_pos = $initialpos;
				my $word_end_pos = pos($str);
				my $length = length $1;
				my $width = mbswidth $1;
				$running_width += $width;
				#Curses::addstr " \\w is '$1', from pos $word_start_pos to $word_end_pos, length $length cp, width $width cols.\n";
				
				# Okay, check if we went past the width limit.
				if ($running_width > $column_to_split_at) {
					# Aha, this word falls across the edge. So break should be at the start of
					# the word. Easy.
					#Curses::addstr "    falls on edge, so break at $word_start_pos.\n";
					return $word_start_pos;
				}
				
			} else {
				pos($str) = $initialpos;	# reset to previous match start, try again.
				if ($str =~ /(\G\W+)/g) {

					my $space_start_pos = $initialpos;
					my $space_end_pos = pos($str);
					my $length = length $1;
					my $width = mbswidth $1;
					$running_width += $width;
					#Curses::addstr " \\W is '$1', from pos $space_start_pos to $space_end_pos, length $length cp, width $width cols.\n";

					# Okay, check if we went past the width limit.
					if ($running_width > $column_to_split_at) {
						# Aha, this nonword falls across the edge. So while potentially not ideal,
						# a break could be at the start of the nonword / the end of the previous word.
						#Curses::addstr "    falls on edge, so break at $space_start_pos.\n";
						return $space_start_pos;
					}

				} else {
					# No match of anything; end of string.
					last;
				}
			}
		}
		# Went across the entire string without falling off edge once,
		# there is nothing to wrap.
		return undef;
	}
	
	
	# Iterate over GCs until we find one beyond the supplied width, for when we need
	# to split a TextFragment in the middle of a word.
	sub get_grapheme_break_point
	{
		my $self = shift;
		my ($column_to_split_at) = @_;
		
		my $running_width = 0;
		my $str = $self->text();
		while (1) {
			my $initialpos = pos($str) // 0;	# Remember pos before regex as we need to put the break before we run out of width.
			if ($str =~ /\G(\X)/g) {
				my $width = mbswidth $1;
				$running_width += $width;
				
				if ($running_width > $column_to_split_at) {
					return $initialpos;
				}
				
			} else {
				last;
			}
		}
		return undef;	# The entire string fits within the columns specified.
	}
	
	
	# Returns two entirely new TextFragments split at the given *CODE POINT*
	# If you want that to be meaningful, get the value from get_break_point(width) first.
	sub split_at_code_point
	{
		my $self = shift;
		my ($cp_to_split_at) = @_;
		
		# It's ok to just link to the same Style object imho, since at this point no-one's
		# going to be fiddling with them (I think...)
		my $tf1 = $self->new(style => $self->style, text => substr($self->text, 0, $cp_to_split_at));
		my $tf2 = $self->new(style => $self->style, text => substr($self->text, $cp_to_split_at));
		return ($tf1, $tf2);
	}
}



# Does text wrapping!
# Feed me a list of TextFragments.
# They correspond to an unbroken line.
# You have already broken things up according to \n characters.
# Call this multiple times if you want a whole 'document' wrapped.
# Tab characters can choke on a dick.

# "Logical" line -> input line, probably much longer than the screen width allows.
# "Physical" line -> output line on screen after wrap-breaks are taken into account.

# Keeps state while accepting new TextFragments.
{
	package Wrapper;
	use Moose;
	use Carp;
	
	# Parameter: How wide do we wrap?
	has 'columns' => qw/is ro isa Int required 1/;
	
	# The current line of TextFragments that is being accumulated.
	has 'curr_line' => qw/is rw isa ArrayRef[TextFragment]/;
	
	# How many columns have been used up by the current line so far.
	has 'curr_width' => qw/is rw isa Int/;
	
	# The collection of all complete lines accumulated so far.
	has 'lines' => qw/is rw isa ArrayRef[ArrayRef[TextFragment]]/;
	
	sub BUILD
	{
		my $self = shift;
		$self->reset();
	}
	
	sub reset
	{
		my $self = shift;
		$self->curr_line([]);
		$self->curr_width(0);
		$self->lines([]);
	}
	
	
	sub add_text_fragment
	{
		my $self = shift;
		my (@tf_queue) = @_;
		
		while (@tf_queue) {
			my $tf = shift @tf_queue;

			my $remaining_columns = $self->columns - $self->curr_width;
			croak "Text::Wrapper fail, curr_width is greater than columns!" if $remaining_columns < 0;
	
			# Determine a suitable break point for this fragment given the remaining columns available.
			my $break_point = $tf->get_word_break_point($remaining_columns);
			
			if ( ! defined $break_point) {
				# $tf does not exceed the available column space.
				$self->_push_text_fragment($tf);
				
			} elsif ($break_point == 0) {
				# The word *right at the start* of this fragment is so huge, it will wrap.
				# This could be a problem if we started this line with this fragment
				# (i.e. at column 0), in which case you'd have to cut it in the middle
				# of a word.
				if (@{$self->curr_line} > 0) {
					# Current line already had some stuff on it.
					# It's still a huge word, but we can just shunt it down a line,
					# Maybe it will fit there?
					$self->newline();
					
					# Someone else can deal with it. i.e. us in the next loop.
					unshift @tf_queue, $tf;
					
				} else {
					# Current line is empty so far.
					# This world too huge, just lop something off the end and be done with it.
					my $chop_point = $tf->get_grapheme_break_point($self->columns);
					my ($tf1, $tf2) = $tf->split_at_code_point($chop_point);
					
					$self->_push_text_fragment($tf1);
					$self->newline();
					
					# ... and let the next loop deal with the trailing part,
					# which may be huge so we won't recurse.
					unshift @tf_queue, $tf2;
					#### Should we make the queue part of the class? Hm.
				}
		
			} else {
				# We have identified a natural word break position to use. It's somewhere after the
				# first word/space of this fragment. The "easy" case.
				my ($tf1, $tf2) = $tf->split_at_code_point($break_point);
				$self->_push_text_fragment($tf1);
				
				# End of (physical) line.
				$self->newline();
				
				# Continue working with the remaining portion of the TextFragment.
				unshift @tf_queue, $tf2;
			}
		}	# End of queue processing.
		# Okay we have added everything!
	}


	sub _push_text_fragment
	{
		my $self = shift;
		my ($tf) = @_;
		
		push @{$self->curr_line}, $tf;
		$self->curr_width( $self->curr_width + $tf->width );
	}	
	
	sub newline
	{
		my $self = shift;
		# Must put curr_line arrayref in a new arrayref, it's going to be reused.
		push @{$self->lines}, [ @{$self->curr_line} ];
		
		$self->curr_line([]);
		$self->curr_width(0);
	}

	#### Currently unused. Might be handy for dealing with \S-\S boundaries.	
	sub previous_text_fragment
	{
		my $self = shift;
		return $self->curr_line->[-1];
	}


	# It would make *sense* for me to go ahead and implement a TextFrame class somewhere
	# that would make calls to the chosen Renderer to actually put characters onto the
	# screen with the appropriate Style etc, having been clipped to a particular region,
	# and all that.
	# For now though, like most things in this 'library', we'll just include a way to dump
	# stuff onto the screen in here completely bypassing encapsulation.
	sub _test_render($$$)
	{
		my $self = shift;
		# Strictly speaking, we don't need to check the width; our wrapping algorithm has already
		# done its job, and is infallible, right?
		my ($frame, $renderer) = @_;
		my $row = 0;
		my $col = 0;
		my $width = $frame->width;
		my $height = $frame->height;
		
		# Work in local coordinates.
		$renderer->set_frame($frame);
		
		# Render logical lines.
		foreach my $line (@{$self->lines}) {
			# Render fragments on that logical line.
			foreach my $frag (@$line) {
				$renderer->string($row, $col, $frag->text);
				$col += $frag->width;
			}
			# Go one line down, if it's within this silly test window anyway.
			$row++;
			$col = 0;
			last unless ($row < $height);
		}
	}
}


1;
